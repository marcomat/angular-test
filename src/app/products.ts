export const products = [
  {
    name: 'Phone XL',
    price: 799,
    description: 'A large phone with one of the best screens'
  },
  {
    name: 'Phone Mini',
    price: 699,
    description: 'A great phone with one of the best cameras'
  },
  {
    name: 'Phone Standard',
    price: 299,
    description: 'Standar Phone'
  },
  {
    name: 'Phone Moto One Action',
    price: 800,
    description: 'Morola One Action with 4 RAM and 5000 mA'
  },
  {
    name: 'Phone Samsung A50',
    price: 1200,
    description: 'Samsung A50 with camera: 13mpx'
  }
];


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/